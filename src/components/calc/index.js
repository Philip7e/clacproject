import { Component } from "react"

import "./calc.css"

class Calc extends Component {
    render() {
        return (
            <>
                <div className="calc">
                    <div className="display"></div>
                    <button className="btn">m+</button>
                    <button className="btn">m-</button>
                    <button className="btn">mrc</button>
                    <button className="btn">C</button>
                    <button className="btn digt">1</button>
                    <button className="btn digt">2</button>
                    <button className="btn digt">3</button>
                    <button className="btn digt">4</button>
                    <button className="btn digt">5</button>
                    <button className="btn digt">6</button>
                    <button className="btn digt">7</button>
                    <button className="btn digt">8</button>
                    <button className="btn digt">9</button>
                    <button className="btn digt">0</button>
                    <button className="btn operator">.</button>
                    <button className="btn operator">+</button>
                    <button className="btn operator">-</button>
                    <button className="btn operator">/</button>
                    <button className="btn operator">*</button>
                    <button className="btn operator">=</button>
                </div>
            </>
        )
    }
}

export default Calc